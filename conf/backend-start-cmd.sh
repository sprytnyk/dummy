python manage.py migrate

gunicorn -b 0.0.0.0 --workers 4 --log-level INFO --timeout 60 core.asgi
